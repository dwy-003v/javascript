let array_con_valores_unicos = [123,345,567,678]


let nombre_parametro_previo = "parametro_5";
let objeto = {
    "parametro" : 1,
    "parametro_2" : 3,
    "parametro_3" : "string",
    [nombre_parametro_previo]: "lo que sea"
}

objeto.parametro_3 = 1;
let obtener = objeto["parametro_3"];

objeto.parametro_5 = "hola";
objeto["parametro_6"] = "chao";
let nombre_parametro = "parametro_7";
objeto[nombre_parametro] = "exacto";

//ESTO ES INVALIDO
objecto[0];
mi_numero = 2;